#define F_CPU 8000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "uart.h"

uint16_t rising1, falling1;
uint16_t rising2, falling2;
uint16_t rising3, falling3;
uint32_t counts1, counts2, counts3;
uint16_t dist1, dist2, dist3;
uint16_t us_per_count;
uint16_t valid_dist = 0;
volatile uint8_t portbhistory = 0xFF;     // default is high because the pull-up
uint8_t sensor = 1;

// Is triggered on timer match of OCR1A
ISR(TIMER1_COMPA_vect)
{
	// Generate a 12us pulse to trigger the HR-SR04
	PORTD ^= (1<<PIN5);
	_delay_us(15);
	PORTD ^= (1<<PIN5);
}

ISR(PCINT_vect)
{
	uint8_t changedbits;
	changedbits = PINB ^ portbhistory;
	portbhistory = PINB;

	if(changedbits & (1 << PB0))
	{
		/* PCINT0 changed */
		if(PINB & (1 << PB0)) {
			rising1 = TCNT1; // Save current count
		}
		else{
			falling1 = TCNT1; // Save current count
			counts1 = (uint32_t)falling1 - (uint32_t)rising1;
			dist1 = (uint32_t)us_per_count * counts1 / 58; // useconds / 58 to get distance in cm
		}
	}

	if(changedbits & (1 << PB2))
	{
	    /* PCINT0 changed */
	    if(PINB & (1 << PB2)) {
	        rising3 = TCNT1; // Save current count
	    }
	    else{
	        falling3 = TCNT1; // Save current count
	        counts3 = (uint32_t)falling3 - (uint32_t)rising3;
	        dist3 = (uint32_t)us_per_count * counts3 / 58; // useconds / 58 to get distance in cm
	    }
	}
	
	//if(changedbits & (1 << PB1))
	//{
		///* PCINT1 changed */
		//if(PINB & (1 << PB1)) {
			//rising2 = TCNT1; // Save current count
		//}
		//else{
			//falling2 = TCNT1; // Save current count
			//counts2 = (uint32_t)falling2 - (uint32_t)rising2;
			//dist2 = (uint32_t)us_per_count * counts2 / 58; // useconds / 58 to get distance in cm
		//}
	//}
}

int main(void)
{
	DDRA = 0xff;
	//PORTA = 0xff; // PORTA PIN 0 as output
	DDRB &= ~(_BV(PCINT0));
	DDRD |= (1<<PIN5); // PORTD PIN 5 as output
	
	// TIMER1 INIT
	TCCR1B |= (1<<ICNC1) | (1<<CS10) | (1<<CS11) | (1<<WGM12); //ICNC1: noise filter, CS10 and CS11: devide clock by 64, WGM12: Clear Timer on Compare (CTC) Mode
	TIMSK |= (1<<OCIE1A); //TICIE1: Timer 1 Input Capture Interrupt Enable, OCIE1A: Output Compare Interrupt Enable 1 A
	// "we suggest to use over 60ms measurement cycle, in order to prevent trigger signal to the echo signal."
	// source: http://www.robosoftsystems.co.in/wikidocs/index.php?title=Ultrasonic_Sensor_(HC-SR04)
	// 70ms cycle: 8MHz/64 = 125000 counts/second => 125000/10 = 12500 counts/100ms => 12500/100*70 = 8750 counts/70ms
	OCR1A = 8750; 
	us_per_count = 8; // 8MHz/64 = 125000 counts/second => 1000000/125000
	
	//// Initiate UART
	//UBRRH = 0;
	//UBRRL = 25; // 25=19200 bps at 8 MHz, 51=9600 bps at 8 MHz
		//
	////UCSRB = _BV(RXEN) | _BV(TXEN);	// Rx Complete Interrupt & Enable Rx/Tx
	//UCSRC = _BV(UCSZ1) | _BV(UCSZ0);	// 8N1
	//// Enable receiver and transmitter and receive complete interrupt
	//UCSRB = ((1<<TXEN)|(1<<RXEN) | (1<<RXCIE));	
	//UCSRC = _BV(UPM1);
	
	PORTB |= ( 1 << PCINT0 ); //turn on pullups
	PCMSK |= ( 1 << PCINT0 ); //enable encoder pins interrupt sources
	PORTB |= ( 1 << PCINT1 ); //turn on pullups
	PCMSK |= ( 1 << PCINT1 ); //enable encoder pins interrupt sources
	PORTB |= ( 1 << PCINT2 ); //turn on pullups
	PCMSK |= ( 1 << PCINT2 ); //enable encoder pins interrupt sources
	
	GIMSK |= ( 1 << PCIE ); //enable pin change interupts
	
	char Buffer[7];
	
	init_uart();
	
	sei();
	
	while (1)
	{
		//PORTA &= ~(1<<PIN0);
		//_delay_us(200000);
		int d1 = dist1;
		int d2 = dist2;
		int d3 = dist3;

		//_delay_us(20000);
		if (d2 < 20)// && valid_dist == 1)
		{
			PORTA |= (1<<PIN1); // Set PIN 0 on PORTB
		}else{
			//if (valid_dist == 1){
			PORTA &= ~(1<<PIN1); // Reset PIN 0 on PORTB
			//}
		}
		
		if (d1 < 20)// && valid_dist == 1)
		{
			PORTA |= (1<<PIN0); // Set PIN 0 on PORTB
		}else{
			PORTA &= ~(1<<PIN0); // Reset PIN 0 on PORTB
		}
		
		if (d3 < 20)// && valid_dist == 1)
		{
			PORTA |= (1<<PIN0);
			PORTA |= (1<<PIN1);
		}

		if (d1 > 400){
		    Buffer[0] = 'M';
		    Buffer[1] = 'A';
		    Buffer[2] = 'X';
		    Buffer[3] = 0x00;
		} else {
		    if (d1 < 2){
		        Buffer[0] = 'M';
		        Buffer[1] = 'I';
		        Buffer[2] = 'N';
		        Buffer[3] = 0x00;
		    } else {
		        itoa(d1,Buffer,10);
		    }
		}

		send_uart(0x02);
		//send_uart('-');
		//send_uart(status);
		int i;
		for(i=0;i<strlen(Buffer);i++){
			send_uart(Buffer[i]);
		}
		//uart_puts(Buffer);
		send_uart(';');
		
		itoa(d2,Buffer,10);
		for(i=0;i<strlen(Buffer);i++){
			send_uart(Buffer[i]);
		}
		//uart_puts(Buffer);

		send_uart(';');
		
		itoa(d3,Buffer,10);
		for(i=0;i<strlen(Buffer);i++){
			send_uart(Buffer[i]);
		}
		//uart_puts(Buffer);
		send_uart(';');


		//send_uart(0x03);
		send_uart(0x0D);
		_delay_us(100000);
		//
		//if (dist > 400){
			//Buffer[0] = 'M';
			//Buffer[1] = 'A';
			//Buffer[2] = 'X';
			//Buffer[3] = 0x00;
		//} else {
			//if (dist < 2){
				//Buffer[0] = 'M';
				//Buffer[1] = 'I';
				//Buffer[2] = 'N';
				//Buffer[3] = 0x00;
			//} else {
				//itoa(dist,Buffer,10);
			//}
		//}
		//USART_Transmit(0x02);
		//USART_putstring(Buffer);
		//USART_Transmit(';');
		//USART_Transmit(0x03);
		//USART_Transmit(0x0D);
	}
}